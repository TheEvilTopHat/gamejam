extends Node

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
#time keeping
var rounds = -1
#global vars
var sheep_count = 0
var sheep_saved = 0
var sheep_killed = 0
var total_sheep_saved = 0
var total_days = 0
var total_points = 0
var wolf_count = 0
var wolf_time = (60*30)
var saved_wolf_time = (60 * 30)
var player = null
var max_y = 1000
var max_x = 1000
var map_cam = null
#gui
var sheepLeftGUI = null
var sheepSavedGUI= null
var sheepKilledGUI = null
var woflsGUI =null
var wolfTimeGUI =null
var dayGUI = null
var scoreGUI = null
var winScreen = null
var loseScreen = null
var shownBol = false
#sheep = 2 + 2 up until 30 sheep then the spawn time is divided by two each day
#load
var wolf_load = null
var sheep_load = null
var player_load = null
func _ready():
	dayGUI = get_node("CanvasLayer/Control/Day")
	scoreGUI = get_node("CanvasLayer/Control/Score")
	winScreen = get_node("CanvasLayer/Control/winPopup")
	loseScreen = get_node("CanvasLayer/Control/losePopup")
	wolf_load = load("scene/wolf.tscn")
	sheep_load = load("scene/sheep.tscn")
	player_load = load("scene/player.tscn")
	rounds = wolf_time
	sheepLeftGUI = get_node("CanvasLayer/Control/SheepLeft")
	sheepSavedGUI= get_node("CanvasLayer/Control/SheepSaved")
	sheepKilledGUI = get_node("CanvasLayer/Control/SheepKilled")
	woflsGUI = get_node("CanvasLayer/Control/Wolfs")
	wolfTimeGUI = get_node("CanvasLayer/Control/WolfTime")
	map_cam = get_node("MapCam")
	player = get_node("Player")
	#player.map_cam = map_cam
	set_fixed_process(true)
	startLevel()

func startLevel():
	#kill all stuff from last level
	#reset player
	player.remove_from_group("player")
	player.free()
	var scene_instance = player_load.instance()
	scene_instance.set_name("Player")
	add_child(scene_instance)
	scene_instance.map_cam = map_cam
	scene_instance.set_pos(Vector2(0,0))
	scene_instance.add_to_group("player")
	player = scene_instance
	var w = get_tree().get_nodes_in_group("wolf")
	for wolf in w:
		if(wolf.close_sheep.get_pos() != get_tree().get_nodes_in_group("far")[0].get_pos()):
			wolf.close_sheep.wolf = null
		wolf.remove_from_group("wolf")
		wolf.free()
		#wolf.queue_free()
	var s = get_tree().get_nodes_in_group("sheep")
	for sheep in s:
		sheep.remove_from_group("sheep")
		sheep.free()
		#sheep.queue_free()
	#spawn sheep
	sheep_count = 2 * (total_days + 1)#1 + pow(2,total_days)
	if(total_days >= 75):
		sheep_count = 150
		wolf_time = int(saved_wolf_time / float(1+((total_days-75)*.5)))
	print("total days " + str(total_days))
	print("NEW SHEEP COUNT = " + str(sheep_count))
	for s in range(sheep_count):
		var scene_instance = sheep_load.instance()
		scene_instance.set_name("sheep")
		add_child(scene_instance)
		scene_instance.par = self
		scene_instance.set_pos(Vector2(rand_range(-max_x,max_x),rand_range(-max_x,max_x)))
	#reset GUI
	sheep_saved = 0
	sheep_killed = 0
	wolf_count = 0
	rounds = wolf_time
func updateGUI():
	sheepLeftGUI.set_text("Sheep Left:"+str(sheep_count))
	sheepSavedGUI.set_text("Sheep Saved:"+str(sheep_saved))
	sheepKilledGUI.set_text("Sheep Killed:"+str(sheep_killed))
	dayGUI.set_text("Day:"+str(total_days + 1))
	scoreGUI.set_text("Score:"+str(total_points))
	woflsGUI.set_text("Wolfs Total:"+str(wolf_count))
	wolfTimeGUI.set_text("Time Till Next Wolf:"+str(rounds/60))
	
func endLevel():
	if(sheep_saved >= sheep_killed and shownBol == false):
		shownBol = true
		#print("YOU WIN")
		rounds = -1
		var text = "Day Completed\n"+"Total Sheep Saved = " \
			+ str(total_sheep_saved) \
			+ " + " \
			+ str(sheep_saved) + " = " + \
			str(int(total_sheep_saved+sheep_saved)) + \
			"\n"+ \
			"Days Completed = " + str(int(total_days + 1)) \
			+"\nScore = " + str(total_points) + " + 100 +" + str(sheep_saved);
		if(sheep_killed == 0):
			text += " * 2 = " + str(total_points + 100 + (sheep_saved * 2)) + "\nPerfect Day!"
			total_points += (2*sheep_saved)
		else:
			text += " = " + str(total_points + 100 + (sheep_saved))
			total_points += sheep_saved
		total_points += 100
		winScreen.get_node("text").set_text(text)
		winScreen.set_hidden(false)
	elif shownBol == false:
		shownBol = true
		rounds = -1
		var text = "Game Over\n"+"Total Sheep Saved = " \
			+ str(total_sheep_saved) \
			+ " + " \
			+ str(sheep_saved) + " = " + \
			str(int(total_sheep_saved+sheep_saved)) + \
			"\n"+ \
			"Days Completed = " + str(int(total_days + 1));
		text += "\nScore = " + str(total_points) + " + " + str(sheep_saved);
		text += " = " + str(total_points + (sheep_saved))
		total_points += sheep_saved
		loseScreen.get_node("text").set_text(text)
		loseScreen.set_hidden(false)
	
func _fixed_process(delta):
	if(rounds > 0):
		rounds -= 1
	if(rounds == 0):
		rounds = wolf_time
		wolf_count += 1
		var s = get_tree().get_nodes_in_group("spawn")
		var r = int(rand_range(0,s.size()))
		#var scene = load("res://scene/wolf.tscn/")
		var scene_instance = wolf_load.instance()
		scene_instance.set_name("wolf")
		add_child(scene_instance)
		scene_instance.rounds = -1
		scene_instance.set_pos(s[r].get_pos())
	updateGUI()
	if(sheep_count <= 0):
		endLevel()
	else:
		pass

func _on_NextDay_pressed():
	shownBol = false
	total_sheep_saved += sheep_saved
	total_days += 1
	winScreen.set_hidden(true)
	startLevel()


func _on_PlayAgain_pressed():
	shownBol = false
	total_sheep_saved = 0
	total_days = 0
	total_points = 0
	wolf_time = saved_wolf_time
	loseScreen.set_hidden(true)
	startLevel()


func _on_Stop_pressed():
	get_tree().change_scene("res://scene/mainMenu.tscn")
