extends Control

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var play = null
func _ready():
	var play = load("res://scene/play.tscn")


func _on_Play_pressed():
	get_tree().change_scene("res://scene/play.tscn")


func _on_Quit_pressed():
	get_tree().quit()


func _on_Help_pressed():
	get_node("HelpPlanel").set_hidden(false)


func _on_Close_pressed():
	get_node("HelpPlanel").set_hidden(true)


