extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var speed = 10; #how fast you move
#var hp = 4; #how long you can live
var pause = false #if the game is paused
var move_up = false
var move_right = false
var move_down = false
var move_left = false
var staff = null
var attack = false
var attack_hit = false
var wait = false
var map_cam = null
var map_cam_bool = false
var cam = null
var col = null
var colA = null
#wall checking code
var sL = false
var sR = false
var sU = false
var sD = false
func attack(): #attack code
	get_node("SamplePlayer").play("attack")
	wait = true
	attack_hit = true
	colA.set_hidden(false)
	print("start")
	staff.set_rotd(270)
	var t = Timer.new()
	t.set_wait_time(.7)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	yield(t, "timeout")
	staff.set_rotd(0)
	t.queue_free()
	var t = Timer.new()
	attack_hit = false
	colA.set_hidden(true)
	t.set_wait_time(.3)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	yield(t, "timeout")
	t.queue_free()
	wait = false
	print("end")

func reset():#called when restarting or starting a level
	pass

func _ready(): #start
	col = get_node("playerBody")
	colA = get_node("playerBody/attack")
	cam = get_node("Camera2D")
	set_fixed_process(true);
	set_process_input(true);
	staff = get_node("staff")

func _input(event): #player input
	if(event.is_action_pressed("map")):
		if(map_cam_bool):
			map_cam.current = false
			map_cam_bool = false
			cam.make_current()
		else:
			map_cam.current = true
			map_cam_bool = true
			cam.current = false
	if(event.is_action_pressed("move_up")):
		move_up = true
	elif(event.is_action_released("move_up")):
		move_up = false
	if(event.is_action_pressed("move_right")):
		move_right = true
	elif(event.is_action_released("move_right")):
		move_right = false
	if(event.is_action_pressed("move_down")):
		move_down = true
	elif(event.is_action_released("move_down")):
		move_down = false
	if(event.is_action_pressed("move_left")):
		move_left = true
	elif(event.is_action_released("move_left")):
		move_left = false
	if(event.is_action_pressed("attack")):
		attack = true
	elif(event.is_action_released("attack")):
		attack = false

func _fixed_process(delta): #do stuff
	sR = false
	sL = false
	sU = false
	sD = false
	if get_tree().get_nodes_in_group("lWall")[0].get_pos().x >= self.get_pos().x:
		print("STOP LEFT")
		sL = true
		sR =false
	elif get_tree().get_nodes_in_group("rWall")[0].get_pos().x <= self.get_pos().x:
		print("STOP RIGHt") 
		sR = true
		sL = false
	if get_tree().get_nodes_in_group("uWall")[0].get_pos().y >= self.get_pos().y:
		print("STOP UP")
		sU = true
		sD =false
	elif get_tree().get_nodes_in_group("dWall")[0].get_pos().y <= self.get_pos().y:
		print("STOP DOWN") 
		sD = true
		sU = false
	if(col.get_overlapping_areas().size() != 0):
		for c in col. get_overlapping_areas():
			for g in c.get_parent().get_groups():
				if g == "wolf" and attack_hit:
					c.get_parent().hit()
				
	var move = Vector2(0,0); #movment vector
	#move code
	if(move_up and sU == false):
		if(self.get_pos().y >= get_tree().get_nodes_in_group("winArea")[0].get_pos().y):
			move.y = -speed #check to make sure not to leave the area
	if(move_right and sR == false):
		self.set_global_scale(Vector2(1,1))
		move.x = speed
	if(move_down and sD == false):
		move.y = speed
	if(move_left and sL == false):
		self.set_global_scale(Vector2(-1,1))
		move.x = -speed
	#action code
	if(attack and wait == false):
		attack()
	#do stuff	
	self.translate(move) #move