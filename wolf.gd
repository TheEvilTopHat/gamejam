extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var player = null
var speed = 7
var r = 0
var playerDistance = 200 #when they start running away from him
var close_sheep
var rounds = 0
var change = 0 #how many teims can switch pos in a secound
var checkTime = (60*10)
var hit = false
var run = false
var runRounds = 0
var col = null
#wall code
var sL = false
var sR = false
var sU = false
var sD = false
func _ready():
	col = get_node("KinematicBody2D")
	find_close_sheep()
	player = get_tree().get_nodes_in_group("player")
	player = player[0]
	set_fixed_process(true)
	

func find_close_sheep():
	var s = get_tree().get_nodes_in_group("sheep")
	var r = get_tree().get_nodes_in_group("far")[0]
	for sheep in s:
		if(sqrt(pow((sheep.get_pos().x - self.get_pos().x),2) + pow((sheep.get_pos().y - self.get_pos().y),2)) < sqrt(pow((r.get_pos().x - self.get_pos().x),2) + pow((r.get_pos().y - self.get_pos().y),2))):
			var yes = true
			for g in sheep.get_groups():
				if g == "dead":
					yes = false
					break
			if(yes):
				r = sheep 
	close_sheep = r

func hit():
	get_node("SamplePlayer").play("hit")
	hit = true
	print("HIT")
	if(close_sheep.get_pos() != get_tree().get_nodes_in_group("far")[0].get_pos()):
		close_sheep.wolf = null
	rounds = checkTime

func _fixed_process(delta):
	#wall detections
	sR = false
	sL = false
	sU = false
	sD = false
	if get_tree().get_nodes_in_group("lWall")[0].get_pos().x >= self.get_pos().x:
		print("STOP LEFT")
		sL = true
		sR =false
	elif get_tree().get_nodes_in_group("rWall")[0].get_pos().x <= self.get_pos().x:
		print("STOP RIGHt") 
		sR = true
		sL = false
	if get_tree().get_nodes_in_group("uWall")[0].get_pos().y >= self.get_pos().y:
		print("STOP UP")
		sU = true
		sD =false
	elif get_tree().get_nodes_in_group("dWall")[0].get_pos().y <= self.get_pos().y:
		print("STOP DOWN") 
		sD = true
		sU = false
	if(col.get_overlapping_areas().size() != 0): #collisoin detection
		for c in col. get_overlapping_areas():
			for g in c.get_parent().get_groups():
				#print(g)
				#print(c.get_parent())
				if g == "player":
					print("P H")
					if(c.get_parent().attack_hit):
						hit()
				#elif g == "sheep" and hit == false:
				#	c.get_parent().die()
	change += 1
	change %= 60 #can swtich once every secound
	if(runRounds > 0):
		runRounds -= 1
	if(runRounds == 0):
		run = false
	if(rounds > 0):
		rounds -= 1
	if(rounds == 0):
		hit = false
		#find_close_sheep()
	
	#if(close_sheep != null):
	#	if(close_sheep.get_pos() != get_tree().get_nodes_in_group("far")[0].get_pos()):
	#		close_sheep.wolf = null
	find_close_sheep()
	var move = Vector2(0,0); #movment vector
	if(sqrt(pow((player.get_pos().x - self.get_pos().x),2) + pow((player.get_pos().y - self.get_pos().y),2)) < playerDistance):
		runRounds = (60*1)
		run = true
	if(run or hit):
		if((abs(player.get_pos().x  - self.get_pos().x) < playerDistance or hit)and player.get_pos().x  < self.get_pos().x ): #player to teh left 
			move.x = speed
		elif((abs(player.get_pos().x  - self.get_pos().x) < playerDistance or hit )and player.get_pos().x  > self.get_pos().x ):#player to the right
			move.x = -speed
		if((abs(player.get_pos().y  - self.get_pos().y) < playerDistance or hit) and player.get_pos().y > self.get_pos().y ):#player to the bot
			move.y = -speed
		elif((abs(player.get_pos().y - self.get_pos().y) < playerDistance or hit) and player.get_pos().y < self.get_pos().y ):#player to the top
			move.y = speed
		
	#check for sheep
	elif(close_sheep.get_pos() != get_tree().get_nodes_in_group("far")[0].get_pos()):
		if(close_sheep.get_pos().x -  self.get_pos().x < 10):
			move.x = -speed
		elif(close_sheep.get_pos().x -  self.get_pos().x > -10):
			move.x = speed
		if(close_sheep.get_pos().y - self.get_pos().y < 10):
			move.y = -speed
		elif(close_sheep.get_pos().y - self.get_pos().y > -10):
			move.y = speed
	else:
		move.x = 0
		move.y = 0
		
	if(move.x > 0 and change == 0):
		self.set_scale(Vector2(1,1))
	elif(move.x < 0 and change == 0):
		self.set_scale(Vector2(-1,1))
	if(move.x > 0 and sR):
		move.x = 0
	if(move.x < 0 and sL):
		move.x = 0
	if(move.y > 0 and sD):
		move.y = 0
	if(move.y < 0 and sU or self.get_pos().y <= get_tree().get_nodes_in_group("winArea")[0].get_pos().y):
		move.y = 0
			
	self.translate(move)