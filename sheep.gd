extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
#visial
var deathSprite = null
var normalSprite = null
#
var wolf = null
var wolf_near = null
var wolf_distance = 400
var player = null
var speed = 4
var r = 4
var far = null
var go = false
var wait = true
var moveTime = 3
var goTimeRunning = false
var playerDistance = 300 #when they start running away from him
var close_sheep
var sheep_distance = 400
var sheep_buffer = 30
var follow = true #follow other sheep
var followTimeRunning = false
var followTimeDur = 1
var rounds = 0
var checkTime = 0
var col = null
var colS = null
var dead = false
var par = null
var win = false
var move = null
var wr = null
#wall code
var sL = false
var sR = false
var sU = false
var sD = false
#rad
var sheepRad = null
var playerRad = null
func followTime():
	var time = followTimeDur
	followTimeRunning = true
	follow = true
	var t = Timer.new()
	t.set_wait_time(time)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	yield(t, "timeout")
	t.queue_free()
	follow = false
	var t = Timer.new()
	t.set_wait_time(time)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	yield(t, "timeout")
	t.queue_free()
	followTimeRunning = false
func goTime(): #the timmer that will keep track of when it should start moving
	var time = moveTime
	goTimeRunning = true
	wait = true
	var t = Timer.new()
	t.set_wait_time(time)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	yield(t, "timeout")
	t.queue_free()
	go = false
	var t = Timer.new()
	t.set_wait_time(time)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	yield(t, "timeout")
	t.queue_free()
	go = true
	wait = false
	goTimeRunning = false
				#if(goTimeRunning == false):
				#	goTime()
	r = int(rand_range(0,5))

func _ready():
	moveTime= rand_range(2,4)
	far = get_tree().get_nodes_in_group("far")[0]
	sheepRad = get_node("Sheep")
	playerRad = get_node("Player")
	normalSprite = get_node("Sprite")
	deathSprite = get_node("Death")
	move = Vector2(0,0)
	col = get_node("KinematicBody2D")
	checkTime = int(rand_range(30,240))
	#find_close_sheep()
	go = true
	wait = false
	moveTime = rand_range(2,4)
	player = get_tree().get_nodes_in_group("player")
	player = null
	#print(player.speed)
	set_fixed_process(true)
	

#func find_close_sheep():
#	var s = get_tree().get_nodes_in_group("sheep")
#	var r = get_tree().get_nodes_in_group("far")[0]
#	for sheep in s:
#		if(sqrt(pow((sheep.get_pos().x - self.get_pos().x),2) + pow((sheep.get_pos().y - self.get_pos().y),2)) < sqrt(pow((r.get_pos().x - self.get_pos().x),2) + pow((r.get_pos().y - self.get_pos().y),2))):
#			if(sheep.get_pos().x != self.get_pos().x and sheep.get_pos().y != self.get_pos().y):
#				r = sheep 
#	close_sheep = r
	#print(close_sheep.get_pos().x)
	
func win():
	add_to_group("dead")
	if(win == false):
		win = true
		get_parent().sheep_count -= 1
		get_parent().sheep_saved += 1
func die():
	if(dead == false):
		normalSprite.set_hidden(true)
		deathSprite.set_hidden(false)
		set_fixed_process(false)
		print("sheed dead")
		get_node("SamplePlayer").play("death")
		dead = true
		#wolf.find_close_sheep()
		add_to_group("dead")
		#self.set_hidden(true)
		self.par.sheep_killed += 1
		self.par.sheep_count -= 1

func other_handle():
	#check for player
		if(player != null):
			if(sqrt(pow((player.get_pos().x - self.get_pos().x),2) + pow((player.get_pos().y - self.get_pos().y),2)) < playerDistance):
				go = false
				if(abs(player.get_pos().x  - self.get_pos().x) < playerDistance and player.get_pos().x  < self.get_pos().x ): #player to teh left 
					move.x = speed
				elif(abs(player.get_pos().x  - self.get_pos().x) < playerDistance and player.get_pos().x  > self.get_pos().x ):#player to the right
					move.x = -speed
				if(abs(player.get_pos().y  - self.get_pos().y) < playerDistance and player.get_pos().y > self.get_pos().y ):#player to the bot
					move.y = -speed
				elif(abs(player.get_pos().y - self.get_pos().y) < playerDistance and player.get_pos().y < self.get_pos().y ):#player to the top
					move.y = speed
				if(goTimeRunning == false):
					goTime()
		#check for sheep
		if(close_sheep != null):
			#if(sqrt(pow((close_sheep.get_pos().x - self.get_pos().x),2) + pow((close_sheep.get_pos().y - self.get_pos().y),2)) < sheep_distance):
			#			#if(follow):
						#	if(followTimeRunning == false):
						#		followTime()
			#	go = false
			#	var moved = false
			#	if(abs(close_sheep.get_pos().x  - self.get_pos().x) < sheep_distance and abs(close_sheep.get_pos().x  - self.get_pos().x) < sheep_buffer - 10 and close_sheep.get_pos().x  < self.get_pos().x ): #player to teh left 
			#		move.x = speed
			#		moved = true
			#	elif(abs(close_sheep.get_pos().x  - self.get_pos().x) < sheep_distance and abs(close_sheep.get_pos().x  - self.get_pos().x) < sheep_buffer - 10 and close_sheep.get_pos().x  > self.get_pos().x ):#player to the right
			#		move.x = -speed
			#		moved = true
			##	if(abs(close_sheep.get_pos().y  - self.get_pos().y) < sheep_distance and abs(close_sheep.get_pos().x  - self.get_pos().x) < sheep_buffer - 10 and close_sheep.get_pos().y > self.get_pos().y ):#player to the bot
				#	move.y = -speed 
			#		moved = true
			#	elif(abs(close_sheep.get_pos().y - self.get_pos().y) < sheep_distance and abs(close_sheep.get_pos().x  - self.get_pos().x) < sheep_buffer - 10 and close_sheep.get_pos().y < self.get_pos().y ):#player to the top
			#		move.y = speed
			#		moved = true
			#	if(moved == false):
			#		if(abs(close_sheep.get_pos().x  - self.get_pos().x) < sheep_distance and abs(close_sheep.get_pos().x  - self.get_pos().x) > sheep_buffer and close_sheep.get_pos().x  < self.get_pos().x ): #player to teh left 
			#			move.x = -speed
			#			moved = true
			#		elif(abs(close_sheep.get_pos().x  - self.get_pos().x) < sheep_distance and abs(close_sheep.get_pos().x  - self.get_pos().x) > sheep_buffer and close_sheep.get_pos().x  > self.get_pos().x ):#player to the right
			#			move.x = speed
			#			moved = true
			#		if(abs(close_sheep.get_pos().y  - self.get_pos().y) < sheep_distance and abs(close_sheep.get_pos().x  - self.get_pos().x) > sheep_buffer and close_sheep.get_pos().y > self.get_pos().y ):#player to the bot
			#			move.y = speed
			#			moved = true
			#		elif(abs(close_sheep.get_pos().y - self.get_pos().y) < sheep_distance and abs(close_sheep.get_pos().x  - self.get_pos().x) > sheep_buffer and close_sheep.get_pos().y < self.get_pos().y ):#player to the top
			#			move.y = -speed
			#			moved = true
			#if to close to other sheep
			#if(rounds == 0 && moved != true):
			if(abs(close_sheep.get_pos().x  - self.get_pos().x) > sheep_buffer and close_sheep.get_pos().x  < self.get_pos().x ): #player to teh left 
				move.x = -speed
			elif(abs(close_sheep.get_pos().x  - self.get_pos().x) > sheep_buffer and close_sheep.get_pos().x  > self.get_pos().x ):#player to the right
				move.x = speed
			if(abs(close_sheep.get_pos().x  - self.get_pos().x) > sheep_buffer and close_sheep.get_pos().y > self.get_pos().y ):#player to the bot
				move.y = speed
			elif(abs(close_sheep.get_pos().x  - self.get_pos().x) > sheep_buffer and close_sheep.get_pos().y < self.get_pos().y ):#player to the top
				move.y = -speed
		if(goTimeRunning == false):
			goTime()
		#random move 
		elif(go):
			#if(wait == false):
			#	moveTime= rand_range(2,4)
				#if(goTimeRunning == false):
				#	goTime()
			#	r = int(rand_range(0,5))
			#	print(r)
			if(r == 0): #left
				move.x = speed
			elif(r == 1): #right
				move.x = -speed
			elif(r == 2): #up
				move.y = speed
			elif(r == 3): #down
				move.y = -speed
			#if 4 then do nothing
func _fixed_process(delta):
	rounds += 1
	rounds %= checkTime
	sR = false
	sL = false
	sU = false
	sD = false
	#sheep detection
	close_sheep = far
	move = Vector2(0,0); #movment vector
	if(get_tree().get_nodes_in_group("winArea")[0].get_pos().y - self.get_pos().y > 0):
		win()
		move.y = -speed
		move.x = 0
	else:
		player = null
		wolf = null
		if(playerRad.get_overlapping_areas().size() != 0):
			for s in playerRad.get_overlapping_areas():
				for g in s.get_parent().get_groups():
					if g == "player":
						player = s.get_parent()
					if g == "wolf":
						wolf = s.get_parent()
		if(sheepRad.get_overlapping_areas().size() != 0):
			for s in sheepRad.get_overlapping_areas():
				for g in s.get_parent().get_groups():
					if g == "sheep":
						if(close_sheep == null and s.get_parent().dead == false):
							close_sheep = s.get_parent()
						else:
							if(sqrt(pow((s.get_pos().x - self.get_pos().x),2) + pow((s.get_pos().y - self.get_pos().y),2)) < sqrt(pow((close_sheep.get_pos().x - self.get_pos().x),2) + pow((close_sheep.get_pos().y - self.get_pos().y),2))):
								if(s.get_parent().dead == false):
									close_sheep = s.get_parent()
		#wall detections
		if(col.get_overlapping_bodies().size() != 0):
				if get_tree().get_nodes_in_group("lWall")[0].get_pos().x >= self.get_pos().x:
					print("S STOP LEFT")
					sL = true
					sR =false
				elif get_tree().get_nodes_in_group("rWall")[0].get_pos().x <= self.get_pos().x:
					print("S STOP RIGHt") 
					sR = true
					sL = false
				if get_tree().get_nodes_in_group("uWall")[0].get_pos().y >= self.get_pos().y:
					print("S STOP UP")
					sU = true
					sD =false
				elif get_tree().get_nodes_in_group("dWall")[0].get_pos().y <= self.get_pos().y:
					print("S STOP DOWN") 
					sD = true
					sU = false
		if(col.get_overlapping_areas().size() != 0 and dead == false): #colision detection
			var wallbol = false
			for c in col.get_overlapping_areas():
				for g in c.get_parent().get_groups():
					if g == "wolf" and c.get_parent().hit == false:
						die()
		var savedWolf = wolf
		if(savedWolf != null): #fix this line
					go = false
					if(savedWolf.get_pos().x  < self.get_pos().x ): #player to teh left 
						move.x = speed
					elif(savedWolf.get_pos().x  > self.get_pos().x ):#player to the right
						move.x = -speed
					if(savedWolf.get_pos().y > self.get_pos().y ):#player to the bot
						move.y = -speed
					elif(savedWolf.get_pos().y < self.get_pos().y ):#player to the top
						move.y = speed
					if(goTimeRunning == false):
						goTime()
		else:
			other_handle()
		if(move.x > 0):
			self.set_scale(Vector2(1,1))
		elif(move.x < 0):
			self.set_scale(Vector2(-1,1))
	if(move.x > 0 and sR):
		move.x = 0
	if(move.x < 0 and sL):
		move.x = 0
	if(move.y > 0 and sD):
		move.y = 0
	if(move.y < 0 and sU):
		move.y = 0
	self.translate(move)